#ifndef TriggerAnalysis_LINKDEF_H
#define TriggerAnalysis_LINKDEF_H

#include <TriggerMatchingTool.h>
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class TriggerMatchingTool;
#endif

#endif
