
# import ROOT in batch mode (no visual output)
import ROOT
ROOT.gROOT.SetBatch(True)

# set ATLAS style
from ROOT import *
import AtlasStyle

# argument parser
import argparse


def drawHisto(name, file, rebin=1):
    c = ROOT.TCanvas()
    h_n_mu = file.Get(name)
    h_n_mu.Rebin(rebin)
    h_n_mu.Draw()
    c.SaveAs('../output/'+name+'.png')
    c.SetLogy(1)
    h_n_mu.Draw()
    c.SaveAs('../output/'+name+'_Log.png')
    c.SetLogy(0)

def getEfficiency(name_numerator, name_denominator, file, name, rebin=1):
    h_numerator = file.Get(name_numerator)
    h_denominator = file.Get(name_denominator)
    h_numerator.Rebin(rebin)
    h_denominator.Rebin(rebin)
    g = ROOT.TGraphAsymmErrors(h_numerator, h_denominator)
    return g

def drawEfficiency(name_numerator, name_denominator, file, name, rebin=1):
    c = ROOT.TCanvas()
    g = getEfficiency(name_numerator, name_denominator, file, name, rebin)
    g.Draw("APE")
    g.GetXaxis().SetRangeUser(2,12)
    g.GetXaxis().SetTitle('p_{T}^{probe} [GeV]')
    g.GetYaxis().SetTitle('Efficiency')
    g.Draw("APE")
    c.SaveAs('../output/efficiency_'+name+'.png')

def drawRate(name_hall, name_hmatch, name_hoff, file, name):
    c = ROOT.TCanvas()
    h_all   = file.Get(name_hall)
    h_match = file.Get(name_hmatch)
    h_match.SetFillColor(ROOT.kBlue)
    h_off   = file.Get(name_hoff)
    h_off.SetFillColor(ROOT.kGreen)

    h_all.Draw()
    h_match.Draw("SAME")
    h_off.Draw("SAME")

    t = ROOT.TLegend(0.38,0.6,0.7,0.9)
    t.SetBorderSize(0)
    t.AddEntry(h_all, 'All L1 RoI', 'f')
    t.AddEntry(h_match, 'RoI matched to probe muon (#DeltaR < 0.3)', 'f')
    t.AddEntry(h_off, 'RoI matched to probe with p_{T} > 4 GeV', 'f')
    t.Draw()

    c.SaveAs('../output/rate_'+name+'.png')

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="input file name")
    args = parser.parse_args()
    filename = args.filename

    f = ROOT.TFile(filename)

    # number of muons
    drawHisto('h_n_mu', f)

    # invariant muon mass spectrum
    drawHisto('h_m_mumu', f)

    # invariant muon mass spectrum centred around J/psi
    drawHisto('h_m_mumu_jpsi', f)

    # tag muon
    drawHisto('h_tag_pt', f)
    drawHisto('h_tag_eta', f)
    drawHisto('h_tag_phi', f)
    drawHisto('h_tag_m', f)
    drawHisto('h_tag_charge', f)
    drawHisto('h_tag_type', f)
    drawHisto('h_tag_dR', f)

    # probe muon
    drawHisto('h_probe_pt', f)
    drawHisto('h_probe_eta', f)
    drawHisto('h_probe_phi', f)
    drawHisto('h_probe_m', f)
    drawHisto('h_probe_charge', f)
    drawHisto('h_probe_type', f)
    drawHisto('h_probe_dR', f)

    # L1 RoI trigger objects
    drawHisto('h_trigger_n', f)
    drawHisto('h_trigger_pt', f)
    drawHisto('h_trigger_eta', f)
    drawHisto('h_trigger_phi', f)
    drawHisto('h_trigger_charge', f)

    # efficiency plots
    drawEfficiency('h_eff_pt_pass', 'h_eff_pt_all', f, 'pt', 5)
    drawEfficiency('h_eff_pt_pass_charge_p', 'h_eff_pt_all_charge_p', f, 'pt_charge_positive', 5)
    drawEfficiency('h_eff_pt_pass_charge_n', 'h_eff_pt_all_charge_n', f, 'pt_charge_negative', 5)

    # rate plots
    drawRate('h_rate_eta_all', 'h_rate_eta_match', 'h_rate_eta_off', f, 'eta')

if __name__ == "__main__":
    main()


