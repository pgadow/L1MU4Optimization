#include "TriggerMatchingTool.h"

#ifndef __CINT__
ClassImp(TriggerMatchingTool);
#endif


TriggerMatchingTool :: TriggerMatchingTool(){
	m_mu_eta = 0;
	m_mu_phi = 0;
	m_mu_pt = 0;
	m_mu_charge = 0;
}
TriggerMatchingTool :: ~TriggerMatchingTool(){}

void TriggerMatchingTool :: setdRMax(float dRMax){
	// set maximum radial distance for muon trigger candidate to muon
	m_dRMax = dRMax;
}
void TriggerMatchingTool :: setMuonInput(std::vector<float> *mu_eta,
										 std::vector<float> *mu_phi,
										 std::vector<float> *mu_pt,
										 std::vector<int> *mu_charge){
	// setting vector pointers in tool to vector pointers in NTuple (and store number of muons)
	m_mu_eta = mu_eta;
	m_mu_phi = mu_phi;
	m_mu_pt = mu_pt;
	m_mu_charge = mu_charge;
}

int TriggerMatchingTool :: getMatchId(float eta, float phi, int charge){
	// match muon using min dR:=(deltaEta^2 + deltaPhi^2)^0.5
	// not yet implemented: pT dependend merging
	float dEta2;
	float dPhi2;
	float dR2;
	float dRMin2 = m_dRMax * m_dRMax;
	unsigned int result = -1;
	for (unsigned int i = 0; i < m_mu_eta->size(); ++i){
		dEta2 = (eta - m_mu_eta->at(i)) * (eta - m_mu_eta->at(i));
		dPhi2 = (phi - m_mu_phi->at(i)) * (phi - m_mu_phi->at(i));
		dR2 = dEta2 + dPhi2;
		if (dR2 < dRMin2){
			dRMin2 = dR2;
			result = i;
 		}
	}
	return result;
}