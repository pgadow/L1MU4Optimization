#include <vector>
#include "TObject.h"
class TriggerMatchingTool : public TObject{
public:
	TriggerMatchingTool();
	~TriggerMatchingTool();
	void setdRMax(float dRMax);
	void setMuonInput(std::vector<float> *mu_eta, std::vector<float> *mu_phi,
				  std::vector<float> *mu_pt,  std::vector<int> *mu_charge);
	int getMatchId(float eta, float phi, int charge);
	
private:
	std::vector<float> *m_mu_eta;
	std::vector<float> *m_mu_phi;
	std::vector<float> *m_mu_pt;
	std::vector<int>   *m_mu_charge;

	// maximum radial distance from muon trigger candidate to offline reconstructed muon
	float m_dRMax = 0.1;

	ClassDef(TriggerMatchingTool,1);
};