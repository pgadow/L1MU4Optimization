#define L1MU4Selector_cxx
// The class definition in L1MU4Selector.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.

// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("L1MU4Selector.C")
// root> T->Process("L1MU4Selector.C","some options")
// root> T->Process("L1MU4Selector.C+")
//

#include "L1MU4Selector.h"
#include <TStyle.h>
#include <TLorentzVector.h>


void L1MU4Selector::Begin(TTree * /*tree*/)
{
   // The Begin() function is called at the start of the query.
   // When running with PROOF Begin() is only called on the client.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

}

void L1MU4Selector::SlaveBegin(TTree * /*tree*/)
{
  // The SlaveBegin() function is called after the Begin() function.
  // When running with PROOF SlaveBegin() is called on each slave server.
  // The tree argument is deprecated (on PROOF 0 is passed).

  TString option = GetOption();

  // initialise trigger list for tag muon
  m_triggerList.insert("HLT_mu4");
  m_triggerList.insert("HLT_mu6");
  m_triggerList.insert("HLT_mu8");
  m_triggerList.insert("HLT_mu10");
  m_triggerList.insert("HLT_mu18");
  m_triggerList.insert("HLT_mu6_idperf");
  m_triggerList.insert("HLT_mu14");
  m_triggerList.insert("HLT_mu20_iloose_L1MU15");
  m_triggerList.insert("HLT_mu20_ivarloose_L1MU15");
  m_triggerList.insert("HLT_mu24_iloose_L1MU15");
  m_triggerList.insert("HLT_mu24_imedium");
  m_triggerList.insert("HLT_mu24_ivarloose_L1MU15");
  m_triggerList.insert("HLT_mu24_iloose");
  m_triggerList.insert("HLT_mu24_ivarloose");
  m_triggerList.insert("HLT_mu24_ivarmedium");
  m_triggerList.insert("HLT_mu26_imedium");
  m_triggerList.insert("HLT_mu26_ivarmedium");
  m_triggerList.insert("HLT_mu28_imedium");
  m_triggerList.insert("HLT_mu28_ivarmedium");
  m_triggerList.insert("HLT_mu18_mu8noL1");
  m_triggerList.insert("HLT_mu20_mu8noL1");
  m_triggerList.insert("HLT_mu22_mu8noL1");
  m_triggerList.insert("HLT_mu24_mu8noL1");
  m_triggerList.insert("HLT_mu26_mu8noL1");
  m_triggerList.insert("HLT_mu4_bJpsi_Trkloose");
  m_triggerList.insert("HLT_mu6_bJpsi_Trkloose");
  m_triggerList.insert("HLT_mu10_bJpsi_Trkloose");
  m_triggerList.insert("HLT_mu18_bJpsi_Trkloose");
  m_triggerList.insert("HLT_mu20_2mu0noL1_JpsimumuFS");
  m_triggerList.insert("HLT_mu18_2mu0noL1_JpsimumuFS");


  // set dR thresholds
  m_dRMaxL1 = 0.3;
  m_dRMaxHLT = 0.1;

  // set up ntuple
  ntuple = new TNtuple("ntuple","ntuple","pt1:eta1:phi1:charge1:type1:dR1:pt2:eta2:phi2:charge2:type2:dR2");

  // set up histograms
  // event information
  h_cutflow = new TH1F("h_cutflow", ";cut;Events", 12, -0.5, 11.5);
  h_n_mu = new TH1F("h_n_mu", ";Number of #mu per event; Entries", 10, -0.5, 9.5);
  h_m_mumu = new TH1F("h_m_mumu", ";m_{#mu#mu}; Entries / 0.1 GeV", 1000, 0., 100);
  h_m_mumu_jpsi = new TH1F("h_m_mumu_jpsi", ";m_{#mu#mu}; Entries / 0.001 GeV", 2000, 2., 4);

  // muon information
  const char *labels[5] = {"Combined", "MuonStandAlone", "SegmentTagged", "CaloTagged", "SiliconAssociatedForwardMuon"};
  const char *triggers[6] = {"MU4", "MU6", "MU10", "MU11", "MU15", "MU20"};
  const char *charges[3] = {"TGC mu^{-}", "TGC mu^{+}", "RPC"};

  // tag muon
  h_tag_pt  = new TH1F("h_tag_pt", ";p_{T}^{tag} [GeV]; Entries / 1 GeV", 100, 0., 100.);
  h_tag_eta = new TH1F("h_tag_eta", ";#eta^{tag}; Entries / 0.1 ", 48, -2.4, 2.4);
  h_tag_phi = new TH1F("h_tag_phi", ";#phi^{tag} [rad]; Entries / 0.1 ", 64, -3.2, 3.2);
  h_tag_m  = new TH1F("h_tag_m", ";m^{tag} [GeV]; Entries / 0.1 GeV", 100, 0.05, 0.15);
  h_tag_charge = new TH1F("h_tag_charge", ";charge^{offl. 1}; Entries", 3, -1.5, 1.5);
  h_tag_type = new TH1F("h_tag_type", ";#tag type; Entries", 5, -0.5, 4.5);
  for (unsigned int i=1;i<=5;i++) h_tag_type->GetXaxis()->SetBinLabel(i,labels[i-1]);
  h_tag_dR = new TH1F("h_tag_dR", ";dR(tag, HLT object); Entries / 0.1", 50,0,0.005);

  // probe muon
  h_probe_pt  = new TH1F("h_probe_pt", ";p_{T}^{probe} [GeV]; Entries / 1 GeV", 100, 0., 100.);
  h_probe_eta = new TH1F("h_probe_eta", ";#eta^{probe}; Entries / 0.1 ", 48, -2.4, 2.4);
  h_probe_phi = new TH1F("h_probe_phi", ";#phi^{probe} [rad]; Entries / 0.1 ", 64, -3.2, 3.2);
  h_probe_m  = new TH1F("h_probe_m", ";m^{probe} [GeV]; Entries / 0.1 GeV", 100, 0.05, 0.15);
  h_probe_charge = new TH1F("h_probe_charge", ";charge^{probe}; Entries ", 3, -1.5, 1.5);
  h_probe_type = new TH1F("h_probe_type", ";#probe type; Entries", 5, -0.5, 4.5);
  for (unsigned int i=1;i<=5;i++) h_probe_type->GetXaxis()->SetBinLabel(i,labels[i-1]);
  h_probe_dR     = new TH1F("h_probe_dR", ";dR(probe, L1 RoI); Entries / 0.1", 50,0,0.5);


  // L1 trigger objects
  h_trigger_n      = new TH1F("h_trigger_n", ";Number of L1 trigger objects; Entries", 10, -0.5, 9.5);
  h_trigger_pt     = new TH1F("h_trigger_pt", ";L1 p_{T} threshold; Entries", 7, -0.5, 6.5);
  for (unsigned int i=2;i<=7;i++) h_trigger_pt->GetXaxis()->SetBinLabel(i,triggers[i-2]);
  h_trigger_eta    = new TH1F("h_trigger_eta", ";L1 #eta; Entries / 0.1 ", 48, -2.4, 2.4);
  h_trigger_phi    = new TH1F("h_trigger_phi", ";L1 #phi [rad]; Entries / 0.1 ", 64, -3.2, 3.2);
  h_trigger_charge = new TH1F("h_trigger_charge", ";L1 charge; Entries", 4, -1.5, 2.5);
  for (unsigned int i=2;i<=4;i++) h_trigger_charge->GetXaxis()->SetBinLabel(i,charges[i-2]);

  // additional histograms
  h_additional_dR_TagProbe    = new TH1F("h_additional_dR_TagProbe", ";dR(probe, tag) Entries / 0.1", 250,0,2.5);
  h_additional_corrL1_pt_dR   = new TH2F("h_additional_corrL1_pt_dR", ";p_{T}^{probe} [GeV];dR(probe, L1 RoI)", 80,0,40,50,0,0.5);

  // histograms for rate measurement
  h_rate_eta_off  = new TH1F("h_rate_eta_off",  ";#eta_{L1};Candidates / 0.06", 84,-2.52,2.52);
  h_rate_eta_match  = new TH1F("h_rate_eta_match",  ";#eta_{L1};Candidates / 0.06", 84,-2.52,2.52);
  h_rate_eta_all  = new TH1F("h_rate_eta_all",  ";#eta_{L1};Candidates / 0.06", 84,-2.52,2.52);

  // histograms for efficiency measurements
  h_eff_pt_pass = new TH1F("h_eff_pt_pass", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);
  h_eff_pt_all  = new TH1F("h_eff_pt_all", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);
  h_eff_pt_pass_charge_p = new TH1F("h_eff_pt_pass_charge_p", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);
  h_eff_pt_all_charge_p  = new TH1F("h_eff_pt_all_charge_p", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);
  h_eff_pt_pass_charge_n = new TH1F("h_eff_pt_pass_charge_n", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);
  h_eff_pt_all_charge_n  = new TH1F("h_eff_pt_all_charge_n", ";p_{T}^{CB} [GeV];Candidates / 0.1", 100, 2, 22);

  // book all objects defined in current directory
  bookOutput();
}

Bool_t L1MU4Selector::Process(Long64_t entry)
{
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // It can be passed to either L1MU4Selector::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the data. When processing
  // keyed objects with PROOF, the object is already loaded and is available
  // via the fObject pointer.
  //
  // This function should contain the "body" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  // count events
  // if (entry%100000 == 0){
  //   std::cout << "Event " << entry << std::endl;
  // }

  // read only branches needed to speed up computing

  // offline reconstructed muons
  b_mu_n->GetEntry(entry);
  b_mu_eta->GetEntry(entry);
  b_mu_phi->GetEntry(entry);
  b_mu_pt->GetEntry(entry);
  b_mu_m->GetEntry(entry);
  b_mu_charge->GetEntry(entry);
  b_mu_muonType->GetEntry(entry);

  // HLT
  b_trigger_info_n->GetEntry(entry);
  b_trigger_info_nTracks->GetEntry(entry);
  b_trigger_info_chain->GetEntry(entry);
  b_trigger_info_isPassed->GetEntry(entry);
  b_trigger_info_typeVec->GetEntry(entry);
  b_trigger_info_etaVec->GetEntry(entry);
  b_trigger_info_phiVec->GetEntry(entry);
  b_trigger_info_ptVec->GetEntry(entry);

  // L1 trigger
  b_trig_L1_mu_n->GetEntry(entry);
  b_trig_L1_mu_eta->GetEntry(entry);
  b_trig_L1_mu_phi->GetEntry(entry);
  b_trig_L1_mu_thrNumber->GetEntry(entry);
  b_trig_L1_mu_charge->GetEntry(entry);
  b_trig_L1_mu_source->GetEntry(entry);

  // event selection: require some muon trigger for tag muon
  // triggerId is the position of the muon trigger in the vector
  h_cutflow->Fill(0);

  int triggerId = -1;
  if(!isTriggered(triggerId))
    return kTRUE;

  // control histogram: number of muons in event
  h_n_mu->Fill(mu_n);

  // event selection: select events with exactly 2 muons
  h_cutflow->Fill(1);
  if (mu_n != 2)
    return kTRUE;

  // step 1: find tag muon closest to trigger
  // tagIdHLT is the position of the tag muon in the vector
  float tag_dR = -1;
  int tagIdHLT = matchHLT(triggerId, tag_dR);

  float tag_pt = -1;
  float tag_eta = -1;
  float tag_phi = -1;
  float tag_m = -1;
  int   tag_charge = -1;
  int   tag_type = -1;

  // match tag muon
  h_cutflow->Fill(2);
  if(tagIdHLT > -1){
    tag_pt   = mu_pt->at(tagIdHLT)*MeVtoGeV;
    tag_eta  = mu_eta->at(tagIdHLT);
    tag_phi  = mu_phi->at(tagIdHLT);
    tag_m    = mu_m->at(tagIdHLT)*MeVtoGeV;
    tag_charge = mu_charge->at(tagIdHLT);
    tag_type   = mu_muonType->at(tagIdHLT);
  }
  else // exit if no tag muon was matched
    return kTRUE;


  // tag muon selection: muon pt > 4 GeV
  h_cutflow->Fill(3);
  if(tag_pt < 4)
    return kTRUE;

  // tag muon selection: abs(muon eta) < 2.4 GeV
  h_cutflow->Fill(4);
  if(abs(tag_eta) > 2.4)
    return kTRUE;

  // tag muon selection: require a combined muon or a segment tagged muon as tag muon
  h_cutflow->Fill(5);
  if ( !(tag_type == 0 || tag_type == 2) )
    return kTRUE;

  // step 2: find probe muon
  // loop over all muons
  for (int j = 0; j < mu_n; ++j){
    float probe_pt   = mu_pt->at(j)*MeVtoGeV;
    float probe_eta  = mu_eta->at(j);
    float probe_phi  = mu_phi->at(j);
    float probe_m    = mu_m->at(j)*MeVtoGeV;
    int probe_charge = mu_charge->at(j);
    int probe_type = mu_muonType->at(j);

    // don't select same probe muon as the tag muon
    if(j == tagIdHLT)
      continue;

    // probe muon selection: only select muons with opposite charge
    h_cutflow->Fill(6);
    if(tag_charge*probe_charge > 0)
      continue;

    // probe muon selection: 1.05 < abs(muon eta) < 2.4 GeV (only select end-cap)
    h_cutflow->Fill(7);
    if( !(abs(probe_eta) > 1.05 && abs(probe_eta) < 2.4) )
      continue;

    // probe muon selection: require a combined or a segment muon for probe muon
    h_cutflow->Fill(8);
    if ( !( probe_type == 0 || probe_type == 2 ) )
      continue;

    // probe muon selection: only select muons with  0.3 < dR(tag, probe) < 2.5
    float dRtagprobe = getdR(tag_eta, tag_phi, probe_eta, probe_phi);
    bool passdRtagprobe = false;
    if( dRtagprobe > 0.1 && dRtagprobe < 2.5)
      passdRtagprobe = true;

    // calculate invariant mass
    float m = getMass(tag_pt, tag_eta, tag_phi, tag_m, probe_pt, probe_eta, probe_phi, probe_m);
    // fill invariant mass spectrum before mass cut
    if(passdRtagprobe)
      h_m_mumu->Fill(m);

    // event selection: require muons from J/Psi decay
    h_cutflow->Fill(9);
    if(!(m > 2.8 && m < 3.34))
      continue;

    // fill additional histograms
    h_additional_dR_TagProbe->Fill(dRtagprobe);

    // apply dR cut
    h_cutflow->Fill(10);
    if(!passdRtagprobe)
      continue;

    h_cutflow->Fill(11);

    // fill invariant mass spectrum around J/psi
    h_m_mumu_jpsi->Fill(m);

    // fill information about tag muons
    h_tag_pt->Fill(tag_pt);
    h_tag_eta->Fill(tag_eta);
    h_tag_phi->Fill(tag_phi);
    h_tag_m->Fill(tag_m);
    h_tag_charge->Fill(tag_charge);
    h_tag_type->Fill(tag_type);
    h_tag_dR->Fill(tag_dR);

    // fill information about probe muons
    h_probe_pt->Fill(probe_pt);
    h_probe_eta->Fill(probe_eta);
    h_probe_phi->Fill(probe_phi);
    h_probe_m->Fill(probe_m);
    h_probe_charge->Fill(probe_charge);
    h_probe_type->Fill(probe_type);



    ////////////////////////
    // TRIGGER ANALYSIS
    ////////////////////////

    h_trigger_n->Fill(trig_L1_mu_n);

    float dRmin = 10;
    bool match = false;
    int charge = -1;
    // loop over L1 trigger objects
    for (int i = 0; i < trig_L1_mu_n; ++i){

      // information about trigger objects
      float roi_eta = trig_L1_mu_eta->at(i);
      float roi_phi = trig_L1_mu_phi->at(i);
      float roi_ptt = trig_L1_mu_thrNumber->at(i);
      int   roi_charge = trig_L1_mu_charge->at(i);
      int   roi_source = trig_L1_mu_source->at(i);


      // only use TGC L1 RoIs
      if( roi_source != 1)
        continue;

      // rate measurement: all trigger RoIs
      h_rate_eta_all->Fill(roi_eta);

      // note: don't apply this for the moment
      // // remove those RoI which can be matched to tag muon within 0.1
      // float dRtag = -1;
      // if(matchROI(tag_eta, tag_phi, tag_pt, roi_eta, roi_phi, dRtag, 0.1))
      //   continue;

      // match object to probe within dR < m_dRMaxL1 = 0.3
      float dR = -1;
      bool roi_match = matchROI(probe_eta, probe_phi, probe_pt, roi_eta, roi_phi, dR, m_dRMaxL1);

      if (roi_match)
        match = true;


      // rate measurement: RoI matched to probe muon
      if (roi_match){
        h_rate_eta_match->Fill(roi_eta);
      }

      // rate measurement: RoI matched to probe muon with pt > 4 GeV
      if(roi_match && probe_pt > 4.){
          h_rate_eta_off->Fill(roi_eta);
      }

      // find smallest dR
      if(dR < dRmin && dR > 0){
        dRmin = dR;
        charge = roi_charge;
      }

      // fill plots for L1 trigger regions of interest (use all objects but those matched to the HLT which fired the event)
      h_trigger_pt->Fill(roi_ptt);
      h_trigger_eta->Fill(roi_eta);
      h_trigger_phi->Fill(roi_phi);
      h_trigger_charge->Fill(roi_charge);
    }

    // minimal distance between probe and trigger objects
    h_probe_dR->Fill(dRmin);
    h_additional_corrL1_pt_dR->Fill(probe_pt, dRmin);


    // efficiency measurement
    if(match)
      h_eff_pt_pass->Fill(probe_pt);
    h_eff_pt_all->Fill(probe_pt);

    if(charge == 1){
      if(match)
        h_eff_pt_pass_charge_p->Fill(probe_pt);
      h_eff_pt_all_charge_p->Fill(probe_pt);
    }
    if (charge == 0){
      if(match)
        h_eff_pt_pass_charge_n->Fill(probe_pt);
      h_eff_pt_all_charge_n->Fill(probe_pt);
    }

  // // fill ntuple
  // ntuple->Fill(pt1,eta1,phi1,charge1,type1,dR1,pt2,eta2,phi2,charge2,type2,dR2);

  }
  return kTRUE;
}

void L1MU4Selector::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}

void L1MU4Selector::Terminate()
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.

  // Define output file
  TFile* output_file = new TFile("output.root", "RECREATE") ;
  // write output to file
  writeOutput(output_file);

}

//////////////////////////
// helper functions
//////////////////////////

bool L1MU4Selector::isTriggered(int& triggerId){
  for (triggerId = 0; triggerId < (int) trigger_info_n; ++triggerId){
    // check if trigger is one of the triggers defined in beginSlave and is passed
    if((m_triggerList.find(std::string(trigger_info_chain->at(triggerId))) != m_triggerList.end()) && trigger_info_isPassed->at(triggerId) == 1){
      //std::cout << trigger_info_chain->at(triggerId) << ": " << trigger_info_isPassed->at(triggerId) << " trigger ID: " << triggerId << std::endl;
      return true;
    }
  }
  return false;
}

float L1MU4Selector::getdR2(float eta1, float phi1, float eta2, float phi2){
  // calculate dR2:=(deltaEta^2 + deltaPhi^2)
  float deta = fabs(eta1 - eta2);
  float dphi = fabs(phi1 - phi2) < TMath::Pi() ? fabs(phi1 - phi2) : 2*TMath::Pi() - fabs(phi1 - phi2);
  return deta*deta + dphi*dphi;
}

float L1MU4Selector::getdR(float eta1, float phi1, float eta2, float phi2){
  // calculate dR:=(deltaEta^2 + deltaPhi^2)^0.5
  return sqrt(getdR2(eta1, phi1, eta2, phi2));
}

int L1MU4Selector::matchHLT(int triggerId, float &dR){
  // match muon using min dR:=(deltaEta^2 + deltaPhi^2)^0.5
  float dR2;
  float dRMin2 = m_dRMaxHLT*m_dRMaxHLT;
  int result = -1;
  int trigger = -1;
  for(int j = 0; j < (int) trigger_info_etaVec->at(triggerId).size(); ++j){
    for (int i = 0; i < mu_n; ++i){
      dR2 = getdR2(trigger_info_etaVec->at(triggerId)[j], trigger_info_phiVec->at(triggerId)[j], mu_eta->at(i), mu_phi->at(i));

      // debug information
      //std::cout << "offline pt / eta / phi: " << mu_pt->at(i) << " "<< mu_eta->at(i) << " " << mu_phi->at(i) << " trigger object pt / eta / phi: " <<  trigger_info_ptVec->at(triggerId)[j] << " " << trigger_info_etaVec->at(triggerId)[j] << " " << trigger_info_phiVec->at(triggerId)[j] << std::endl;

      if (dR2 < dRMin2 ){
        dRMin2 = dR2;
        result = i;
        trigger = j;
      }
    }
  }

  // debug information
  if(result > -1 && trigger > -1)
    //std::cout << "match to trigger found with offline pt / eta / phi: " << mu_pt->at(result) << " "<< mu_eta->at(result) << " " << mu_phi->at(result) << " trigger object pt / eta / phi: " <<  trigger_info_ptVec->at(triggerId)[trigger] << " " << trigger_info_etaVec->at(triggerId)[trigger] << " " << trigger_info_phiVec->at(triggerId)[trigger] << std::endl;

  dR = sqrt(dRMin2);
  return result;
}


bool L1MU4Selector::matchROI(float probe_eta, float probe_phi, float probe_pt, float roi_eta, float roi_phi, float &dR, float dRmax){
  // match muon using min dR:=(deltaEta^2 + deltaPhi^2)^0.5
  // not yet implemented: pT dependend matching

  // get dR threshold (if unphysical: use predefined value)
  if(dRmax < 0)
    dRmax = m_dRMaxL1; // might be replaced by pt dependent function

  // calculate dR
  dR = getdR(probe_eta, probe_phi, roi_eta, roi_phi);

  if (dR < dRmax){
    return true;
  }

  return false;
}



float L1MU4Selector::getMass(float pt1, float eta1, float phi1, float m1,
                             float pt2, float eta2, float phi2, float m2){
  TLorentzVector p1, p2;
  p1.SetPtEtaPhiM(pt1, eta1, phi1, m1);
  p2.SetPtEtaPhiM(pt2, eta2, phi2, m2);
  return (p1+p2).M();
}

void L1MU4Selector::prettyPrint(std::string preamble, std::string data, int width, char separator){
  std::cout << std::setw(width) << preamble << separator << " " << data << std::endl;
}

void L1MU4Selector::bookOutput(){

  // register objects
  TList* obj_list = (TList*) gDirectory->GetList() ;
  TIter next_object((TList*) obj_list) ;
  TObject* obj ;
  std::cout << "-- Booking objects:" << std::endl;
  while ((obj = next_object())) {
    TString objname = obj->GetName() ;
    std::cout << " " << objname << std::endl ;
    fOutput->Add(obj) ;
  }
}

void L1MU4Selector::writeOutput(TFile* output_file){

  // Retrieve objects
  TList* list = GetOutputList() ;
  TIter next_object((TList*) list);
  TObject* obj ;
  std::cout << "-- Retrieved objects:" << std::endl ;
  output_file->cd();
  while ((obj = next_object())) { TString objname = obj->GetName() ; std::cout << " " << objname << std::endl ; obj->Write() ; }

  // Write output file
  output_file->Write() ;
}