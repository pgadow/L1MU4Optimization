## L1 MU4 trigger optimization

This work is done as part of the qualification task of Philipp Gadow (paul.philipp.gadow@cern.ch).

The code revolves around TGC NTuples, which are documented here:
[https://twiki.cern.ch/twiki/bin/view/Main/L1TGCNtuple](https://twiki.cern.ch/twiki/bin/view/Main/L1TGCNtuple).

The analysis code used PROOF-Lite to maximize the computing speed by parallel computing and uses the TSelector for looping over the events.